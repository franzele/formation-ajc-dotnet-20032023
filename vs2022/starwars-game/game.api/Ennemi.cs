﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace game.api
{
    /// <summary>
    /// Ennemi que le joueur va rencontrer
    /// </summary>
    public class Ennemi : BasePersonnage
    {
        private static Random _Random = new Random();

        public override void SeDeplacer()
        {
            int nouveauX = this.Position.X + Ennemi._Random.Next(-1, 1);
            int nouveauY = this.Position.Y + Ennemi._Random.Next(-1, 1);

            this.Position = new Position2D(nouveauX, nouveauY);
        }
    }
}
