﻿using decouverte_delegates;

void CalculerSomme(params int[] values)
{
    int result = values.Sum();
    Console.WriteLine(result);
}

CalculerSomme(1, 2, 5, 6, 7, 8, 10, 15);

#region Etude de cas d'un delegue
Afficher afficher = Console.WriteLine;
afficher("salut");
#endregion