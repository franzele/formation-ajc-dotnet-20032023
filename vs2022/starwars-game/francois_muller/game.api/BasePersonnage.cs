﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace game.api
{
    /// <summary>
    /// Classe parente des persos => vous devez créer une classe enfante pour instancier
    /// </summary>
    public abstract class BasePersonnage
    {
        #region Fields
        private static readonly Random _random = new();
        public const int DEFAULT_POINTS_DE_VIE = 100;

        private int pointsDeVie = DEFAULT_POINTS_DE_VIE;
        #endregion

        #region Public methods
        /// <summary>
        /// Déplacement du personnage en 2D
        /// L'abstract oblige les classes enfants à override, 
        /// on n'a pas le choix, sinon ça compile pas
        /// </summary>
        public abstract void SeDeplacer();

        /// <summary>
        /// Fait baisser les points de vie de l'autre personnage
        /// </summary>
        /// <param name="personnage"></param>
        /// <exception cref="Exceptions.PersonnageIdentiqueAttenduException">On peut pas attaquer soi-même ;)</exception>
        public void Attaquer(BasePersonnage ennemi)
        {
            if (this == ennemi)
            {
                throw new Exceptions.PersonnageIdentiqueAttenduException();
            }

            int pointsAttaque = this.DefinirPointsAttaque();
            ennemi.PerdreVie(pointsAttaque);
        }

        /// <summary>
        ///  Je définis une méthode qui sera spécifiable par les enfants
        ///  uniquement pour cette partie de code
        /// </summary>
        /// <returns></returns>
        protected virtual int DefinirPointsAttaque()
        {
            return BasePersonnage._random.Next(0, this.Force);
        }

        // Abstract pour obliger la définition du comportement (du code) dans l'enfant
        //protected abstract int DefinirPointsAttaque();


        /// <summary>
        /// Met à jour les points de vie
        /// </summary>
        /// <param name="value"></param>
        protected void PerdreVie(int value)
        {
            // this.PointsDeVie = this.PointsDeVie - value;
            this.PointsDeVie -= value;
        }

        /// <summary>
        /// Se protèger, et récupérer
        /// </summary>
        /// <param name="personnage"></param>
        public virtual void SeDefendre(BasePersonnage personnage)
        {
            throw new NotImplementedException();
        }
        #endregion

        #region Properties
        public int PointsDeVie
        {
            get => this.pointsDeVie;
            set
            {
                this.pointsDeVie = value;
                if (this.pointsDeVie < 0)
                {
                    this.pointsDeVie = 0;
                }
            }
        }

        public bool EstVivant => this.PointsDeVie > 0;

        public int Force { get; set; }

        public int Bouclier { get; set; }

        public Position2D Position { get; set; } = new Position2D(0, 0);
        #endregion
    }
}
