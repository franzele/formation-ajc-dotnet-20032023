﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace game.api
{
    /// <summary>
    /// Classe représentant le joueur dans le jeu
    /// </summary>
    public class Player
    {
        private List<Game> gameList = new();



        /// <summary>
        /// List en lecture seule des parties du joueur
        /// </summary>
        public List<Game> GameList { get => gameList; private set => gameList = value; }
    }
}
