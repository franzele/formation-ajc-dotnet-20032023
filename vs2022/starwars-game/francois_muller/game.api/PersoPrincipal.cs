﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace game.api
{
    /// <summary>
    /// Personnage principale, avec des spécificités sur l'attaque
    /// </summary>
    public class PersoPrincipal : BasePersonnage
    {
        private readonly Action<string> afficherInformation;
        private readonly Func<string> recupererSaisie;
        private static readonly Dictionary<string, Position2D> deplacements = new()
        {
            { "Q", new (-1, 0) },
            { "D", new (1, 0) },
            { "Z", new (0, -1) },
            { "S", new (0, 1) }
        };

        public PersoPrincipal(Action<string> afficherInformation, Func<string> recupererSaisie)
        {
            this.afficherInformation = afficherInformation;
            this.recupererSaisie = recupererSaisie;
        }

        //public override void Attaquer(BasePersonnage ennemi)
        //{
        //    base.Attaquer(ennemi);
        //}

        public override void SeDeplacer()
        {
            this.afficherInformation("Direction ? (QZSD)");
            string saisie = this.recupererSaisie();
            if (saisie != null)
            {
                var vecteur = this.ConvertirPosition(saisie);
                this.Position = new(this.Position.X + vecteur.X, this.Position.Y + vecteur.Y);
            }
        }

        private Position2D ConvertirPosition(string direction)
        {
            return PersoPrincipal.deplacements[direction];
        }

        protected override int DefinirPointsAttaque()
        {
            var pointsAttaque = base.DefinirPointsAttaque();

            pointsAttaque += this.Bouclier;

            return pointsAttaque;
        }

        public event Action<PersoPrincipal> estMort;

        public void Meurt(PersoPrincipal perso)
        {
            this.estMort?.Invoke(this);
        }
    }
}
