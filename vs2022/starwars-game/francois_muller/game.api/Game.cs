﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace game.api
{
    /// <summary>
    /// Représente une partie de jeu, avec un perso, des check points, ...
    /// </summary>
    public class Game
    {
        #region Public methods
        /// <summary>
        /// Demarrer une nouvelle partie
        /// Crée un nouveau perso, ...
        /// </summary>
        public void Demarrer()
        {
            this.DateDebut = DateTime.Now;
        }

        /// <summary>
        /// Créer un point de sauvegarde
        /// </summary>
        /// <exception cref="NotImplementedException">Plante quand ......</exception>
        public void Sauvegarder()
        {
            if (this.PersoPrincipal != null)
            {
                CheckPoint checkPoint = new(this.Id, this.PersoPrincipal.PointsDeVie);
                this.CheckPointList.Add(checkPoint);
            }
        }

        public void FinDePartie()
        {
            Console.WriteLine("Fin De Partie");
        }
        #endregion

        #region Properties
        public int Id { get; set; } = 0;

        public List<CheckPoint> CheckPointList { get; set; } = new();

        public DateTime DateCreation { get; private set; } = DateTime.Now;

        public DateTime DateDebut { get; private set; } = DateTime.Now;

        public BasePersonnage? PersoPrincipal { get; set; }
        #endregion
    }
}
