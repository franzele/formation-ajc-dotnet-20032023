﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace menu.api
{
    /// <summary>
    /// Un item du menu
    /// </summary>
    public class MenuItem
    {
        public int Id { get; set; }

        public string Libelle { get; set; } = "";

        public int OrdreAffichage { get; set; }

        public override string ToString()
        {
            return $"{this.Id} : {this.Libelle}";
        }
    }
}
