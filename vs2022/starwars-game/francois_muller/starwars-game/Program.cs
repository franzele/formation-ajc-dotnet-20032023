﻿
using game.api;
using menu.api;
using starwars_game;

//Position2D position = new() { X = 2, Y = 2 };
//Position2D position2 = new() { X = 2, Y = 2 };

BasePersonnage persoPrincipal = new PersoPrincipal(Console.WriteLine, Console.ReadLine);
//BasePersonnage persoPrincipal = new Ennemi();

//while (true)
//{
//    persoPrincipal.SeDeplacer();
//}

Menu menu = new(mess =>
{
    Console.ForegroundColor = ConsoleColor.DarkGreen;
    Console.WriteLine(mess);
    Console.ForegroundColor = ConsoleColor.White;
},
Console.ReadLine);

var game = new Game();
game.Demarrer();

// Je vais attaquer

try
{
    game.Sauvegarder();
    // Je ferme
}
catch (NotImplementedException ex)
{
    Console.ForegroundColor = ConsoleColor.Green;
    Console.WriteLine("Oops erreur !", ex.Message);
    Console.ForegroundColor = ConsoleColor.White;
}
catch (Exception ex)
{

}
finally
{
    Console.WriteLine("Execute obligatoire");
}
//catch (Exception ex)
//{
//    Console.ForegroundColor = ConsoleColor.Red;
//    Console.WriteLine("Oops erreur", ex.Message);
//    Console.ForegroundColor = ConsoleColor.White;
//}




Console.ForegroundColor = ConsoleColor.DarkGreen;

// string monTitre = "a starwars game".ToUpper();
var monTitre = "a starwars game".ToUpper();

// monTitre = monTitre + " (" + DateTime.Now.ToString() + ")";
//string format = "{0} ({1})";
//monTitre = string.Format(format, monTitre, DateTime.Now);
//Console.WriteLine(monTitre);

// Code identique
// Console.WriteLine(format, monTitre, DateTime.Now);

Console.WriteLine($"{monTitre.ToUpper()} ({DateTime.Now.ToString("yyyy.MM.dd dddd")}) !");
Console.ForegroundColor = ConsoleColor.DarkCyan;
var sousTitre = "A zelda copy cat";
Console.WriteLine(sousTitre);
Console.ForegroundColor = ConsoleColor.White;


void AfficherMenu()
{
    //string[] menu = { // new string[] 
    //    "1. Nouvelle partie",
    //    "2. Charger partie",
    //    "0. Quitter"
    //};

    //foreach (var item in menu)
    //{
    //    Console.WriteLine(item);
    //}

    var listMenu = Enum.GetValues(typeof(MenuItemType));
    foreach (var item in listMenu)
    {
        string itemTransforme = item.ToString().Replace("_", " ");
        Console.WriteLine("{0}: {1}", (int)item, itemTransforme);
    }

    //Type type = typeof(string);
    //foreach(var item in type.GetMethods())
    //{
    //    Console.WriteLine(item);
    //}
}

int ChoisirMenu()
{
    int menuItem = 0;

    do
    {
        AfficherMenu();

        Console.WriteLine("Ton choix ?");
        menuItem = int.Parse(Console.ReadLine()); // TODO: Voir pour utiliser un try parse => ou gestion des exceptions
        // const int DEMARRER_PARTIE_INDEX = 1;
        //        MenuItemType monChoix = MenuItemType.Quitter;

        MenuItemType monChoix = (MenuItemType)menuItem;
        switch (monChoix)
        {
            case MenuItemType.Demarrer_Partie:
                {
                    DemarrerPartie(); // Ici, exceptions ?
                }
                break;

            case MenuItemType.Charger_Partie:
                {
                    ChargerPartie();
                }
                break;

                // TODO: il manque le default ?
        }
    } while (menuItem != 0);

    return menuItem;
}

void DemarrerPartie()
{
    Console.WriteLine("Ton prénom stp ?");
    var prenom = Console.ReadLine();

    // DateTime maVraiDate;
    bool dateValide = false;
    do
    {
        Console.WriteLine("Ta date de naissance stp ?");
        var dateDeNaissance = Console.ReadLine();

        if (DateTime.TryParse(dateDeNaissance, out var maVraiDate))
        {
            var comparaisonDates = DateTime.Now - maVraiDate;
            int nbAnnees = (int)(comparaisonDates.TotalDays / 365);
            Console.WriteLine($"Ton prénom est bien ? {prenom}, et tu es âgé de {nbAnnees} années ?");

            dateValide = true;
            int ageSaisi = DemanderAge();
        }
        else
        {
            Console.ForegroundColor = ConsoleColor.DarkRed;
            Console.WriteLine("Erreur de saisie de date, ré-essaie stp !");
            Console.ForegroundColor = ConsoleColor.White;
        }
    }
    while (!dateValide);
}

int DemanderAge()
{
    bool ageValide = false;
    int vraiAge = 0;

    while (!ageValide)
    {
        Console.WriteLine("Ton age stp ?");
        var age = Console.ReadLine();

        ageValide = int.TryParse(age, out vraiAge);

        if (ageValide)
        {
            ageValide = vraiAge > 13;
        }
    }

    return vraiAge;
}

void ChargerPartie()
{

}


// PreparerEnnemis();
// ChoisirMenu();


void PreparerEnnemis()
{
    //string[] nomAs =
    //{
    //    "Dark Vador",
    //    "Boba fet",
    //    "Empereur Palpatine"
    //};
    // string[][] tableauTableau; // tableau avec des sous tableaux de dimension différentes
    //var item = tableauTableau[0][0];

    string[,] multiDimensionnel = new string[2, 3];
    var item = multiDimensionnel[0, 0];

    //noms[0] = "Darth Vader";

    List<string> noms = new List<string>()
    {
        "Dark Vador",
        "Boba fet",
        "Empereur Palpatine",
    };
    //noms.Add("Droide");
    //noms.Remove("Droide");

    //var enemi = noms[0];
    //int index = noms.IndexOf("Boba fet");
    //enemi = noms[index];


    //List<int> powers = new List<int>()
    //{
    //    1, 2, 5, 10, 20
    //};

    //var sum = powers.Sum(); // Linq
    //var first = powers.Min();

    //var min = nomAs.Min();
    DemandeAjoutEnnemisParUtilisateur(noms);

    var query = from enem in noms
                let eneMaj = enem[0].ToString().ToUpper() + enem.Substring(1).ToLower()
                let premierCar = enem[0]
                let monObjet = new { Maj = eneMaj, Initial = enem }
                where enem.StartsWith("D") || enem.EndsWith("D")
                orderby enem descending
                select monObjet;

    foreach (var enemy in query)
    {
        Console.WriteLine(enemy.Maj);
        Console.WriteLine(enemy.Initial);
    }

    //var monObjALaVolee = new // On peut créer un objet sans type, à la volée (il a un type anonyme)
    //{
    //    Prenom = "Anakin"
    //};
    //Console.WriteLine(monObjALaVolee.Prenom);

    // Linq avec methods
    //var query2 = noms.Where(item => item.StartsWith("D") && item.EndsWith("D"))
    //                 .OrderByDescending(item => item)
    //                 .Select(item => item);

    //query = query.Where(item => item.EndsWith("E"));


}

void DemandeAjoutEnnemisParUtilisateur(List<string> lesEnnemis)
{
    const string ARRET_SAISIE = "STOP";
    string valeurSaisie = "";
    bool demandeArret = false;

    do
    {
        Console.WriteLine("Nouvel ennemi ? (STOP pour arrêter)");
        valeurSaisie = Console.ReadLine();

        //var index = lesEnnemis.IndexOf(valeurSaisie);
        //if( index >= 0)
        //{

        //}
        // if (! lesEnnemis.Exists()) => sera vu avec la notion de delegués
        demandeArret = valeurSaisie == ARRET_SAISIE;

        if (!demandeArret && !lesEnnemis.Contains(valeurSaisie))
        {
            lesEnnemis.Add(valeurSaisie);
        }

    } while (!demandeArret);

}

Menu menu1 = new Menu(Console.WriteLine, Console.ReadLine);

MenuItem quitter = new MenuItem();
quitter.OrdreAffichage = 3;
quitter.Id = 3;
quitter.Libelle = "Quitter le jeu";

MenuItem nouvelle_partie = new MenuItem();
nouvelle_partie.OrdreAffichage = 1;
nouvelle_partie.Id = 1;
nouvelle_partie.Libelle = "Créer une nouvelle partie";

MenuItem charger_partie = new MenuItem();
charger_partie.OrdreAffichage = 2;
charger_partie.Id = 2;
charger_partie.Libelle = "Charger une ancienne partie";

menu1.Ajouter(quitter);
menu1.Ajouter(charger_partie);
menu1.Ajouter(nouvelle_partie);

menu1.Afficher();
