﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace tva.api.Calculateurs
{
    public class TVACalculator
    {
        /*public List<decimal> FiltrerTVA(List<decimal> tvas, decimal filtre = 0.055M)
        {
            return tvas.Where(item )
        }*/
        public void InitialserListe(List<decimal> tvas, RecupererSaisie recupererSaisie,
                                                        AfficherInformation afficher)
        {
            bool arretSaisie = false;

            do
            {
                var saisieUtilisateur = recupererSaisie();

                arretSaisie = saisieUtilisateur == "STOP";
                if(! arretSaisie )
                {
                    if (decimal.TryParse(saisieUtilisateur, out var tva))
                    {
                        tvas.Add(tva);
                    }
                }

            } while (!arretSaisie);
        }

        public decimal CalculerTTC(decimal montantHT, decimal tva)
        {
            return montantHT * (1 + tva);
        }



        public List<decimal> FiltrerTVA(List<decimal> tvas, decimal filtre = 0.055M)
        {
            //var query = from tva in tvas
            //            where tva >= filtre
            //            select tva;

            return tvas.Where(item => item >= filtre).ToList();
        }


    }
        
}
