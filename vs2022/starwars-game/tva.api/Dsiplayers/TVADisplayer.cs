﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tva.api.Dsiplayers
{
    /// <summary>
    /// Affiche la liste des tvas
    /// </summary>
    /// <param name="tvas">Liste de tvas, venant du programme</param>
    public class TVADisplayer
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="tvaList"></param>
        public void Display(List<Decimal> tvaList, AfficherInformation afficher)
        {
            var query = from tva in tvaList
                        select new
                        {
                            TVABrut = tva,
                            TVAEuro = $"{tva} €"
                        };
            foreach (var item in query)
            {
                afficher(item);
            }
        }
    }
}
