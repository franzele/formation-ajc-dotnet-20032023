﻿using zoo.api;

/* // avant héritage
var maydine = new Animal(AnimalType.Gorille, "Maydine", true);
var animal2 = new Animal(AnimalType.Elephant, "Dumbo", true);*/

var maydine = new Gorille("Maydine", false);
var arthur = new Perroquet("arthur", true);

Console.WriteLine(maydine.Name);
maydine.Attaquer(arthur);