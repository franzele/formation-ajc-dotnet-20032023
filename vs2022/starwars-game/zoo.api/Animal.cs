﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zoo.api
{
    public class Animal
    {
        private decimal poids = 10;
        private string habitat = "";
        private string name = "";
        private int pv;
        private int force;
        private bool rarete;
        private string zone = "";
        private int portee;
        private bool hostile;
        private string son = "";


        public Animal() : this("", false) { }

        public Animal(string name, bool hostile) : this(name, 10)
        {
            this.hostile = hostile;
        }

        public Animal(string name, decimal poids, bool rarete = false)
        {
            this.Name = name;

            this.Poids = poids;
        }


        public decimal Poids
        {
            get { return poids; }
            set
            {
                if (this.poids < 0)
                {
                    this.poids = 10;
                }
                this.poids = value;
            }
        }

        public int PositionX { get; set; } = 10;

        public string Habitat { get => habitat; set => habitat = value; }

        public string Name { get => name; set => name = value; }
        public int PV { get => pv; set => pv = value; }

        public bool EstEnVie => this.PV > 0;
        public bool EstEnVie2 { get { return this.PV > 0; } }

        public int Force { get => force; set => force = value; }
        public bool Rarete { get => rarete; set => rarete = value; }
        public string Zone { get => zone; set => zone = value; }
        public int Portee { get => portee; set => portee = value; }
        public bool Hostile { get => hostile; set => hostile = value; }
        public string Son { get => son; set => son = value; }

        public virtual void Attaquer(Animal animal)
        {
            Console.WriteLine("attaque en cours");
        }
    }
}
