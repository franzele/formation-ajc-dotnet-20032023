﻿// See https://aka.ms/new-console-template for more information
using AutourTVA;
using System.Diagnostics;

/*Console.OutputEncoding = System.Text.Encoding.UTF8;

const char monnaie = '€';

Afficher afficher = Console.WriteLine;
Afficher afficherVert = afficherTVASuperieur;

void afficheListe(Afficher item, List<float> listeTVA)
{
    foreach (var TVA in listeTVA)
    {
        item($"{TVA} / {TVA} \u20AC");
    }
    
}

void afficheTVAHaut(List<float> listeTVA)
{
    var query = from tva in listeTVA
                where tva > 1.055
                orderby tva ascending
                select tva;

    List<float> tmpTVASuperieur = new List<float>();

    foreach (var item in query)
    {
        tmpTVASuperieur.Add(item);
    }
    afficheListe(afficherVert, tmpTVASuperieur);
}

void afficherTVASuperieur(object item)
{
    Console.ForegroundColor = ConsoleColor.Green;
    Console.WriteLine(item);
    Console.ForegroundColor = ConsoleColor.Gray;

}

void CalculTTC(float prixHT, float TVA, char monnaie)
{
    var nouvPrixHT = prixHT + (TVA * prixHT / 100);
    Console.WriteLine ($"{nouvPrixHT} {monnaie}");
}

List<float> listeTVA = new List<float>();

bool continuerAjoutTVA = true;


do
{
    float nouvTVA;
    Console.WriteLine("Veuillez ajouter des TVA (STOP pour arreter)");
    var entree = Console.ReadLine();
    var estFloat = float.TryParse(entree, out nouvTVA);

    if (estFloat != true)
    {
        if (entree.ToUpper() == "STOP")
        {
            Console.WriteLine("Fin des ajouts des TVA");
            continuerAjoutTVA = false;
        }
        else
        {
            Console.WriteLine("Veuillez rentrer une valeur de TVA ou STOP");
        }
    }
    else
    {
        if (listeTVA.IndexOf(nouvTVA) == -1)
        {
            listeTVA.Add(nouvTVA);
        }
        else
        {
            Console.WriteLine($"La valeur TVA {nouvTVA} existe déjà!");
        }
    }

    afficheListe(afficher, listeTVA);
} while (continuerAjoutTVA);

afficherTVASuperieur(listeTVA);

CalculTTC(1, 0, monnaie);
CalculTTC(1, 20, monnaie);*/

using tva.api;
using tva.api.Calculateurs;
using tva.api.Dsiplayers;

// TvaCalculateur calculateur = new TvaCalculateur();
TVACalculator calculateur = new();
TVADisplayer displayer = new();

List<decimal> tvaList = new();

void AfficherEnVert(object message)
{
    Console.ForegroundColor = ConsoleColor.Green;
    Console.WriteLine(message);
    Console.ForegroundColor = ConsoleColor.White;
}

// calculateur.InitialiserListe(tvaList, Console.ReadLine, Console.WriteLine);
// calculateur.InitialiserListe(tvaList, Console.ReadLine, AfficherEnVert);
// calculateur.InitialiserListe(tvaList, Console.ReadLine, AfficherEnVert);


AfficherInformation afficher = Console.WriteLine;
afficher("blala");

afficher = AfficherEnVert;
afficher("blala");

var tvasFiltrees = calculateur.FiltrerTVA(tvaList);
var tvasFiltrees2 = calculateur.FiltrerTVA(tvaList, 0.056M);
